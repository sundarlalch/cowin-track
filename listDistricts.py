import json
import sys,getopt
import requests
import time
import winsound

def main(argv):

  stateId='16'

  opts, args=getopt.getopt(argv,"p:v:d:a:",['vaccine=,"date=","age='])
  for opt,arg in opts:
    if opt == '-d':
      stateId=arg
  uri="https://cdn-api.co-vin.in/api/v2/admin/location/districts/"+str(stateId)
  headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15'} 
  response=requests.get(uri,headers=headers)
  centers_json= response.json()
  for i in centers_json['districts']:
    print ( i["district_id"] , " - "+i["district_name"] )
  
if __name__ == "__main__":
  main(sys.argv[1:])