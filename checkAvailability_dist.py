import json
import sys,getopt
import requests
import time
import winsound


def main(argv):

  from datetime import date
  today = date.today()
  date=today.strftime("%d-%m-%Y")
  age=''

  # Change the district code
  dist='265'
  
  try:
   opts, args=getopt.getopt(argv,"p:v:d:a:",['vaccine=,"date=","age='])
  except getopt.GetoptError:
    print('checkAvailability.py -a <18 for 18+ and 45 for 45+>')
    sys.exit(2)
  
  for opt,arg in opts:
   if opt == '-a':
    age=arg
   elif opt == '-d':
    dist=arg
  uri="https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict?district_id="+str(dist)+"&date="+date
  print (uri)
  headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15'} 
  while True:
    response=requests.get(uri,headers=headers)
  #  print (response)
    centers_json= response.json()
  
    for i in centers_json['centers']:
      for j in i["sessions"]:
        if j["available_capacity_dose1"] > 0  and j["min_age_limit"] == int(age): 
          print ("available_capacity :", j["available_capacity"] , " date :"+j["date"] ," center :"+i["name"] ," PIN :"+str(i["pincode"]))
          print ("------------------\n")
    time.sleep(60)
if __name__ == "__main__":
  main(sys.argv[1:])
