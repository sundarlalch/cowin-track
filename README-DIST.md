install packages - Execute following commands to install pre requistites 
================
python3 - installs Python 3
pip install requests - insalls requests package

You can find your  District code by executing 
======================================================
list_states.bat - will list the states with it's state code.
list_districts.bat <state code> - lists the districts 

Command to run
=================
//For 18+
python3 checkAvailability_dist.py  -a 18 [-d district code]

Or
//For 45+
python3 checkAvailability_dist.py -d 20-05-2021 -a 45 [-d district code]